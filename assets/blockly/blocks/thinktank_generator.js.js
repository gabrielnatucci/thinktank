Blockly.JavaScript['sensor_vision'] = function(block) {
  var dropdown_visual_elements = block.getFieldValue('VISUAL_ELEMENTS');
  // TODO: Assemble JavaScript into code variable.
  var code = '...';
  // TODO: Change ORDER_NONE to the correct strength.
  // return [code, Blockly.JavaScript.ORDER_NONE];
  return code;
};

Blockly.JavaScript['logic_if_vision'] = function(block) {
  var value_visual_qty = Blockly.JavaScript.valueToCode(block, 'VISUAL_QTY', Blockly.JavaScript.ORDER_ATOMIC);
  var statements_if_statements = Blockly.JavaScript.statementToCode(block, 'IF_STATEMENTS');
  var statements_else_statements = Blockly.JavaScript.statementToCode(block, 'ELSE_STATEMENTS');
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.JavaScript['control_gameloop'] = function(block) {
  var statements_name = Blockly.JavaScript.statementToCode(block, 'NAME');
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.JavaScript['control_tank_forward'] = function(block) {
  var dropdown_tank_direction = block.getFieldValue('TANK_DIRECTION');
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.JavaScript['control_tank_fire'] = function(block) {
  var dropdown_power = block.getFieldValue('POWER');
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.JavaScript['control_rotate_gun'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.JavaScript['get_enemy_direction'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = '...';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['math_negate'] = function(block) {
  var value_value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = '...';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['tank_variables'] = function(block) {
  var dropdown_name = block.getFieldValue('NAME');
  // TODO: Assemble JavaScript into code variable.
  var code = '...';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_NONE];
};