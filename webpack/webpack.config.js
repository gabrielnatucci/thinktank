const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const Dotenv = require('dotenv-webpack');


const codePath = path.join(__dirname, '..');
module.exports = {
  entry: {
    app: path.resolve(codePath, 'src/index.ts')
    //static: glob.sync(path.resolve(codePath, 'public/*'))
  },
  mode: process.env.NODE_ENV || 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.join(codePath, 'dist'),
    compress: true,
    host: '0.0.0.0',
    port: 9000
  },
  output: {
    pathinfo: true,
    path: path.resolve(codePath, 'dist'),
    filename: '[name].js'
  },
  watch: true,
  plugins: [
    new Dotenv({
      path: path.resolve(path.join(__dirname, '..'), '.env.example'),
      systemvars: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'typeof CANVAS_RENDERER': JSON.stringify(true),
      'typeof WEBGL_RENDERER': JSON.stringify(true)
    }),
    new CopyWebpackPlugin([{
      from: 'assets',
      to: 'assets'
    }]),
    // Make sure that the plugin is after any plugins that add images
    new ImageminPlugin({
      disable: process.env.NODE_ENV !== 'production', // Disable during development
      pngquant: {
        quality: '95-100'
      }
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(codePath, 'src/index.html')
    }),
  ],
  module: {
    rules: [{
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.js$/,
        use: ['babel-loader'],
        include: path.join(codePath, 'src')
      },
      {
        test: [/\.vert$/, /\.frag$/],
        use: 'raw-loader'
      },
      {
        test: /\.(gif|png|jpe?g|svg|xml)$/i,
        use: "file-loader"
      },
      {
        test: /\.(s)css$/,
        exclude: [/node_modules/],
        use: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader'
        }]
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json'],
    alias: {
      assets: path.join(codePath, '/assets'),
      static: path.join(codePath, '/static'),
    },
  },
};