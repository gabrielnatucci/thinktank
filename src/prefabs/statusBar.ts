/// <reference path="../../types/phaser.d.ts" />

interface statusBarConfig {
  scene: Phaser.Scene,
  x: number,
  y: number,
  total: number,
  height: number,
  backgroundColor: number,
  statusColor: number
}

export default class StatusBar extends Phaser.GameObjects.Rectangle {
  scene: Phaser.Scene
  valueBar: Phaser.GameObjects.Rectangle
  totalValue: number
  currentValue: number

  constructor(config: statusBarConfig) {
    super(config.scene, config.x, config.y, config.total, config.height)
    this.scene = config.scene
    this.totalValue = config.total
    this.currentValue = this.totalValue
    this.setFillStyle(config.backgroundColor, 1)
    // Creates the actual bar that is going to be used
    this.valueBar = new Phaser.GameObjects.Rectangle(this.scene, config.x, config.y, this.width, this.height)
    this.valueBar.setFillStyle(config.statusColor, 1)
    config.scene.add.existing(this)
    config.scene.add.existing(this.valueBar)
    this.setDepth(10)
    this.valueBar.setDepth(15)
  }

  public dropStatus(value: number) {
    const tweenConfig = {
      targets: this.valueBar,
      width: Math.max(this.valueBar.width - value, 0),
      ease: "Linear",
      duration: 100,
      repeat: 0,
      yoyo: false,
      delay: 0,
      onComplete: () => {
        this.currentValue = this.valueBar.width
        if (this.currentValue) {
          this.setAlpha(0.8)
        }
      }
    }
    this.scene.tweens.add(tweenConfig)
  }

  public updateStatus(value: number) {
    const tweenConfig = {
      targets: this.valueBar,
      width: Math.max(value, 0),
      ease: "Linear",
      duration: 100,
      repeat: 0,
      yoyo: false,
      delay: 0,
      onComplete: () => {
        this.currentValue = this.valueBar.width
        if (this.currentValue) {
          this.setAlpha(0.8)
        }
      }
    }
    this.scene.tweens.add(tweenConfig)
  }

  public updatePosition(x: number, y: number) {
    this.setX(x)
    this.setY(y)
    this.valueBar.setX(x)
    this.valueBar.setY(y)
  }

  destroy() {
    this.valueBar.destroy()
    super.destroy()
  }

}