import 'phaser'
import Bullet from './bullet'
import PlayerStatus from './playerStatus'
import * as Matter from 'matter-js';
import BlocklyElement from './BlocklyElement';
import { Data } from 'phaser';
import { isNullOrUndefined } from 'util';

function createObjectId() {
  let timestamp = (new Date().getTime() / 1000 | 0).toString(16)
  return timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, function() {
      return (Math.random() * 16 | 0).toString(16)
  }).toLowerCase()
}
interface playerProperties {
  atlas: string
  x: number
  y: number
  scene: Phaser.Scene
  world: Phaser.Physics.Matter.World
  sprite: string
  turretSprite: string
  trailSprite: string,
  socket: SocketIOClient.Socket,
  isMultiplayer: boolean
  id?: string
}

interface turretProperties {
  atlas: string
  x: number
  y: number
  scene: Phaser.Scene
  world: Phaser.Physics.Matter.World
  sprite: string,
  group: number
}

interface serializedPlayerTurret {
  rotation: number,
  sprite: string
}

interface serializedPlayerStatus {
  isDestroyed: boolean
  energyDepleted: boolean
  currentHealth: number
  currentPower: number
}

export interface serializedPlayer {
  id?: string
  x: number
  y: number
  rotation: number
  sprite: string
  trail: string
  turret: serializedPlayerTurret
  status: serializedPlayerStatus
}

export class Turret extends Phaser.Physics.Matter.Sprite {
  body: Matter.Body
  size: number
  private trackedObjects: any[] = []
  public objectData: any[] = []
  public sensorOn: boolean = true

  constructor(config: turretProperties) {
    // Renders the sprite
    super(config.world, config.x, config.y, config.atlas, config.sprite)
    config.scene.add.existing(this)
    const canvasHeight =  config.scene.sys.canvas.height
    const canvasWidth = config.scene.sys.canvas.width
    this.size = Math.sqrt(Math.pow(canvasHeight, 2) + Math.pow(canvasWidth, 2))
    this.setRectangle(this.displayWidth, this.size * 2, {isSensor: true})
    this.setDepth(1.5)
    this.setSensor(true)
    this.setCollisionCategory(Number(process.env.TURRET_COLLISION_GROUP))
    this.setCollisionGroup(config.group)
    this.setCollidesWith([Number(process.env.PLAYER_COLLISION_GROUP)])
    this.setOriginFromFrame()
    this.updateDisplayOrigin()
  }

  public updatePosition(x: number, y: number) {
    this.x = x
    this.y = y
    // Keeps tank from rotating when colliding with objects, while keeping its inertia
    this.setAngularVelocity(0)
    // Changes the data analyzed
    this.analyzeObjects()
  }

  public updateRotation(dt: number, x?: number, y?: number, rotation?: number) {
    const step = dt * 0.001 * 2; // convert to sec
    let targetAngle: number
    if (x !== undefined && y !== undefined) {
      targetAngle =  Phaser.Math.Angle.Between(this.x, this.y, x, y) - (Math.PI / 2)
      targetAngle = Phaser.Math.Angle.Wrap(targetAngle)
    } else if (rotation !== undefined) {
      targetAngle = rotation
    }
    this.rotation = Phaser.Math.Angle.RotateTo(this.rotation, targetAngle, step)
    // Changes the data analyzed
    this.analyzeObjects()
    return targetAngle
  }

  public trackObject(object: Phaser.Physics.Matter.Sprite | Player) {
    if (!this.trackedObjects.includes(object)) {
      this.trackedObjects.push(object)
    }
  }

  public forgetObject(object: Phaser.Physics.Matter.Sprite | Player) {
    this.trackedObjects = this.trackedObjects.filter((obj) => obj !== object)
  }

  public cleanSensor() {
    this.trackedObjects = []
    this.objectData = []
  }

  public hasObjectInSight(tracking_type:string):boolean {
    if (!isNullOrUndefined(this.objectData)) {
      for (let i = 0; i < this.objectData.length; i++) {
        const obj_sensor = this.objectData[i];
        console.log(obj_sensor['type']+" === "+tracking_type)
        if (obj_sensor['type'] === tracking_type) {
          return true;
        }
      }
    }
    return false;
  }

  public analyzeObjects() {
    if (this.sensorOn) {
      this.objectData = this.trackedObjects.map((obj) => {
        let data = {}
        if (obj instanceof Player) {
          if (!obj.isDestroyed) {
            data = {...data, ...{health:obj.status.currentHealth(), energyDepleted: obj.energyDepleted, type: "player"}}
          }
        } else if (obj instanceof Bullet) {
          data = {...data, ...{type: "projectile"}}
        } else {
          data = {...data, ...{type: "obstacle"}}
        }
        data = {...data, ...{x: obj.x, y: obj.y}}
        return data
      })
    }
  }
}

export class Player extends Phaser.Physics.Matter.Sprite {
  id: string
  turret: Turret
  config: playerProperties
  speed: number = 3
  isMoving: boolean = false
  nextFire: number = 0
  fireRate: number = 250
  status: PlayerStatus
  isNPC: boolean
  energyDepleted: boolean = false
  isDestroyed: boolean = false
  isMultiplayer: boolean = false
  body: Matter.Body
  socket: SocketIOClient.Socket
  private animatedDeath: boolean = false

  fullCode:string;
  currentLine:number = 0;
  blockCodeDelay = 0;

  constructor(config: playerProperties, isNPC: boolean = false) {
    // Renders the sprite
    super(config.world, config.x, config.y, config.atlas, config.sprite)
    config.scene.add.existing(this)
    // Sets the multiplayer configuration, if needed
    this.isMultiplayer = config.isMultiplayer
    if (config.id !== undefined) {
      this.id = config.id
    }
    this.setDepth(1)
    const group = -1*Phaser.Math.FloatBetween(1, 100)
    this.setBounce(0)
    this.setFriction(1)
    this.setCollisionCategory(Number(process.env.PLAYER_COLLISION_GROUP))
    this.setCollisionGroup(group)
    //this.setCollidesWith([Number(process.env.BOUNDS_COLLISION_GROUP),Number(process.env.PLAYER_COLLISION_GROUP), Number(process.env.BULLET_COLLISION_GROUP)])
    // Creates a turret
    this.turret = new Turret({
      x:this.x,
      y: this.y,
      atlas: config.atlas,
      sprite: config.turretSprite,
      world: config.world,
      scene: config.scene,
      group: group
    })
    // Saves the parameter variables
    this.config = config
    this.isNPC = isNPC
    // Fix pivot point for turret and tank sprites
    this.setOriginFromFrame()
    this.updateDisplayOrigin()
    // Automatically creates a trail for the tank
    setInterval(this.createTrail, 400, this)
    // Sets the user status bar, name and energy
    this.status = new PlayerStatus(this.scene, this.x, this.y, Phaser.Display.Color.HexStringToColor('#25313f').color, 100)
    // Player multiplayer configuration
    if(this.isMultiplayer) {
      this.socket = config.socket
      // Configures all socket callbacks
      if (!this.isNPC) {
        setInterval(() => this.socket.emit('playerTankMovement', {velocity: this.body.velocity, rotation: this.rotation}), 100)
        setInterval(() => this.socket.emit('playerUpdated', this.serializePlayer()), 100)
      }
    }

  }

  private createTrail(context: Player) {
    if (context.isMoving) {
      // Generates the tank trail
      const trail = context.config.scene.matter.add.image(context.x, context.y, context.config.atlas, context.config.trailSprite)
      trail.angle = context.angle
      trail.setStatic(true)
      trail.setDepth(0)
      trail.setCollisionCategory(Number(process.env.GROUND_COLLISION_GROUP))
      trail.setCollidesWith(0)
      setTimeout(()=> {
        trail.destroy()
      }, 2000)
    }
  }

  public updateRotation(dt: number, targetAngle?: number) {
    const dir = this.body.velocity
    const step = dt * 0.001 * 2; // convert to sec
    if(!targetAngle) {
      targetAngle = Phaser.Math.Angle.Between(this.x, this.y, this.x + 100*(dir.x/this.speed), this.y + 100*(dir.y/this.speed)) + (Math.PI / 2)
      // flip the angle another 180 degrees, since the sprite 'front' is face down
      targetAngle += Math.PI
      targetAngle = Phaser.Math.Angle.Wrap(targetAngle)

      // Update the rotation smoothly.
      if (Math.abs(dir.x) > 0.05 || Math.abs(dir.y) > 0.05) {
        this.rotation = Phaser.Math.Angle.RotateTo(this.rotation, targetAngle, step)
      }
    } else {
      this.rotation = Phaser.Math.Angle.RotateTo(this.rotation, targetAngle, step)
    }
  }
  private updateTurretPos() {
    this.turret.updatePosition(this.x, this.y)
  }

  public updateTurretRotation(dt: number, x?: number, y?: number, rotation?: number) {
    const targetAngle = this.turret.updateRotation(dt, x, y, rotation)
    if (!this.isNPC && this.isMultiplayer) {
      this.socket.emit('playerTurretTurn', targetAngle)
    }
  }

  private fireExplosion(isHeavy: boolean = false) {
    let bulletCoil: Phaser.Physics.Matter.Sprite
    let offsetCoil = new Phaser.Geom.Point(0, this.turret.height*1.2)
    Phaser.Math.Rotate(offsetCoil, this.turret.rotation)
    if (isHeavy) {
      bulletCoil = this.config.scene.matter.add.sprite(this.turret.x + offsetCoil.x, this.turret.y + offsetCoil.y, this.config.atlas, 'towerDefense_tile298.png')
    } else {
      bulletCoil = this.config.scene.matter.add.sprite(this.turret.x + offsetCoil.x, this.turret.y + offsetCoil.y, this.config.atlas, 'towerDefense_tile297.png')
    }
    bulletCoil.setDepth(1)
    bulletCoil.setCollidesWith(0)
    bulletCoil.rotation = this.turret.rotation
    bulletCoil.flipY = true
    bulletCoil.setScale(0.5)
    bulletCoil.setVelocity(this.body.velocity.x, this.body.velocity.y)
    // Uses the animation explosion
    const tweenConfig = {
      targets: bulletCoil,
      scaleX: 0.8,
      scaleY: 0.8,
      ease: "Linear",
      duration: 100,
      repeat: 0,
      yoyo: false,
      delay: 0,
      onComplete: () => {
        bulletCoil.setStatic(true)
        bulletCoil.destroy()
      }
    }
    this.config.scene.tweens.add(tweenConfig)

  }

  private depleteTank() {
    this.scene.sound.play('snd_no_energy')
    const randomPos = Phaser.Math.RandomXY(new Phaser.Math.Vector2(0,0), 5)
    const smoke = this.config.scene.matter.add.sprite(this.x + randomPos.x, this.y + randomPos.y, this.config.atlas, 'explosionSmoke5.png')
    this.config.scene.add.existing(smoke)
    smoke.setDepth(10)
    smoke.setStatic(true)
    smoke.setCollidesWith(0)
    smoke.setScale(0.1,0.1)
    const tweenConfig = {
      targets: smoke,
      scaleX: 0.8,
      scaleY: 0.8,
      rotation: smoke.rotation + 2*Phaser.Math.PI2,
      ease: "Quad",
      duration: 2000,
      repeat: 0,
      yoyo: false,
      delay: 0,
      onUpdate: () => {
        const randomPos = Phaser.Math.RandomXY(new Phaser.Math.Vector2(0,0))
        this.x += randomPos.x
        this.y += randomPos.y
        this.updateTurretPos()
      },
      onComplete: () => {
        smoke.destroy()
      }
    }
    this.config.scene.tweens.add(tweenConfig)
    // Darkens tank sprite
    const tintColor = Phaser.Display.Color.HexStringToColor('#888888').color
    this.turret.tint = tintColor
    this.tint = tintColor
  }

  public rotateByDeltaAngle(deltaAngle:number, dt:number, turret:boolean = false) {
    var baseAngle = this.angle;
    if (turret) {
      baseAngle = this.turret.angle;
    }
    var finalAngle = (baseAngle + deltaAngle) * Math.PI/180.0
    this.rotateByAngle(finalAngle, dt, turret);
  }

  public rotateByAngle(targetAngle: number, dt: number, turret: boolean = false) {
    const step = dt * 0.001 * 2; // convert to sec
    targetAngle = Phaser.Math.Angle.Wrap(targetAngle)
    if (turret) {
      this.turret.rotation = Phaser.Math.Angle.RotateTo(this.turret.rotation, targetAngle, step)
    } else {
      this.rotation = Phaser.Math.Angle.RotateTo(this.rotation, targetAngle, step)
    }
  }

  public moveForward() {
    // Creates the bullet velocity and sets it
    const vel = Phaser.Math.Rotate(new Phaser.Geom.Point(0,this.speed), this.rotation)
    this.setVelocity(vel.x, vel.y)
    this.updateTurretPos()
    if (vel.x !== 0 || vel.y !== 0) {
      this.isMoving = true
    }
  }

  public stopMovement() {
    this.setVelocity(0,0)
    this.isMoving = false
  }

  public fireBullet(time: number, isHeavy: boolean = false) {
    if (time >= this.nextFire && !this.energyDepleted && !this.isDestroyed) {
      if (!this.isNPC && this.isMultiplayer) {
        this.socket.emit('playerBulletFired', isHeavy)
      }
      this.fireExplosion(isHeavy)
      // creates the bullet
      let offset = new Phaser.Geom.Point(0, 1.5*this.turret.height)
      Phaser.Math.Rotate(offset, this.turret.rotation)
      const bulletConfig = {
        x: this.turret.x + offset.x,
        y: this.turret.y + offset.y,
        isHeavy: isHeavy,
        rotation: this.turret.rotation,
        atlas: this.config.atlas,
        scene: this.config.scene,
        world: this.config.world,
        tankType: 'none'

      }
      new Bullet(bulletConfig)
      // Updates the next fire bullet rate
      this.nextFire = time + this.fireRate
      // Deplete the energy shot
      let energy = 2
      if (isHeavy) {
        energy *=2
        this.scene.sound.play('snd_shot02')
      } else {
        this.scene.sound.play('snd_shot01')
      }
      this.status.dropPower(energy)
      if (this.status.currentPower() <= 0) {
        this.energyDepleted = true
      }
    }
  }

  public takeHit(bullet: Bullet) {
    if (!this.energyDepleted) {
      let damage = 5
      if (bullet.isHeavy) {
        damage *=2
      }
      this.status.dropHealth(damage)
      if (this.status.currentHealth() <= 0) {
        this.isDestroyed = true
      }
      if (this.isDestroyed) {
        this.setDisplaySize(this.width*2, this.height*2)
        this.setCollidesWith(0)
        this.turret.setVisible(false)
        this.turret.setCollidesWith(0)
        this.anims.play('tankExplosion')
        setTimeout(() => {
          // Removes all the tank sprites
          this.setVisible(false)
          this.setStatic(true)
          this.setCollisionCategory(Number(process.env.GROUND_COLLISION_GROUP))
          this.turret.setCollisionCategory(Number(process.env.GROUND_COLLISION_GROUP))
          this.turret.setStatic(true)
          // Creates a sign on the ground after the explosion
          const trail = this.config.scene.matter.add.image(this.x, this.y, this.config.atlas, 'towerDefense_tile019.png')
          trail.setDisplaySize(trail.width*1.5, trail.height*1.5)
          trail.angle = this.angle
          trail.setStatic(true)
          trail.setDepth(0)
          trail.setCollidesWith(0)
        }, 1000)
      }
    }
  }

  public serializePlayer(): serializedPlayer {
    return {
      x: this.x,
      y: this.y,
      rotation: this.rotation,
      sprite: this.config.sprite,
      trail: this.config.trailSprite,
      turret: {rotation: this.turret.rotation, sprite: this.config.turretSprite},
      status: {
        isDestroyed: this.isDestroyed,
        energyDepleted: this.energyDepleted,
        currentHealth: this.status.currentHealth(),
        currentPower: this.status.currentPower()
      }
    }

  }

  /**
   * Executa o código de blocos passado. Teoricamente, um comando por segundo.
   * @param time - tempo total decorrido na fase
   * @param delta - tempo desde o último frame
   */
  blockExecution(time: number, delta: number) {
    this.fullCode = BlocklyElement.blockly.getCode(this);
    this.blockCodeDelay += delta;
    let cadence = 100;
    if (this.blockCodeDelay > cadence) {
      this.blockCodeDelay = 0
      // console.log("Last executed line: "+this.currentLine+" of "+this.blockCode.length);
      // console.info("full code: "+this.fullCode)
      // if (this.blockCode.length > 0) {
      //   this.currentLine = (this.currentLine + 1) % this.blockCode.length
      //   eval(this.blockCode[this.currentLine])
      //   console.log("code: "+this.blockCode[this.currentLine])
      // }
      eval(this.fullCode);
    }
  }

  update(time: number, delta: number) {
    const cursors = this.scene.input.keyboard.createCursorKeys()
    const pointer = this.scene.input.activePointer

    if (this.energyDepleted && !this.animatedDeath) {
      this.depleteTank()
      this.animatedDeath = true
    }
    // updates the status bar position
    this.status.updateStatusPosition(this.x + 100, this.y - 50)
    // Keeps tank from rotating when colliding with objects, while keeping its inertia
    this.setAngularVelocity(0)
    // Adjusts the turret position based on the the tank body
    this.updateTurretPos()
    if (this.isNPC) {
      if (this.isMultiplayer) {
        this.updateRotation(delta)
      } else {
        if (!this.energyDepleted && !this.isDestroyed) {
          this.blockExecution(time, delta);
        }
      }
    }
    if (!this.isNPC && !this.energyDepleted && !this.isDestroyed) {
      // Updates turret position based on mouse location
      const turretX = this.scene.input.mousePointer.x
      const turretY = this.scene.input.mousePointer.y
      this.updateTurretRotation(delta, turretX, turretY)
      let velocityUpdate: number
      if (cursors.up.isDown)
      {
        velocityUpdate = -1*this.speed
        this.setVelocityY(velocityUpdate)
        this.isMoving = true
      }
      else if (cursors.down.isDown)
      {
        velocityUpdate = this.speed
        this.setVelocityY(velocityUpdate)
        this.isMoving = true
      }
      else if (cursors.up.isUp && cursors.down.isUp)
      {
        this.setVelocityY(0)

      }
      if (cursors.left.isDown)
      {
        velocityUpdate = -1*this.speed
        this.setVelocityX(velocityUpdate)
        this.isMoving = true
      }
      else if (cursors.right.isDown)
      {
        velocityUpdate = this.speed
        this.setVelocityX(velocityUpdate)
        this.isMoving = true
      }
      else if (cursors.right.isUp && cursors.left.isUp)
      {
        this.setVelocityX(0)
      }
      this.updateRotation(delta)
      if (cursors.up.isUp && cursors.down.isUp && cursors.right.isUp && cursors.left.isUp) {
        this.isMoving = false
      }

      if (cursors.space.isDown || pointer.leftButtonDown()) {
        this.fireBullet(time)
      }
      if (pointer.rightButtonDown()) {
        this.fireBullet(time, true)
      }
    }
  }
  destroy() {
    this.turret.destroy()
    this.status.health.destroy()
    this.status.power.destroy()
    super.destroy()
  }
}