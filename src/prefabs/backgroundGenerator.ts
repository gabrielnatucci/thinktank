import 'phaser'
import {getRandomItem, getRandomInt, transpose} from '../helpers/math'

interface generatorProperties {
  tileWidth: number
  tileHeight: number
  tilesheetPath: string
  spritesheetAtlas: string
  gridWidth: number
  gridHeight: number
  scene: Phaser.Scene
}
interface objectInfo {
  x: number
  y: number
  rotation: number
  sprite: string
}
interface objectBackground {
  below: objectInfo[]
  level: objectInfo[]
  above: objectInfo[]
}

export interface serializedBackground {
  floor: number[][]
  objects: objectBackground

}
export class BackgroundGenerator{
  sceneConfig: generatorProperties
  backgroundTypes = { MIXED: 2, SAND: 1, GRASS: 0}
  orientationTypes = {VERTICAL: 1, HORIZONTAL: 0}
  mixedOrientation: number
  splitScenario: number = 0
  level: number[][]
  map: Phaser.Tilemaps.Tilemap
  belowPlayer: Phaser.GameObjects.Group
  abovePlayer: Phaser.GameObjects.Group
  levelPlayer: Phaser.GameObjects.Group

  constructor(settings: generatorProperties) {
    this.sceneConfig = settings
    this.create()

  }
  // TODO: Create a generic way of passing the tile groups
  private generateRow(backgroundType: number, arraySize: number): number[] {
    let row: number[]
    if (backgroundType === this.backgroundTypes.MIXED) {
      // Generate a mixed row division
      if (this.mixedOrientation === this.orientationTypes.VERTICAL) {
        row = Array(arraySize).fill(22)
      } else {
        row = Array(arraySize).fill(24)
      }
    } else if (backgroundType === this.backgroundTypes.SAND) {
      // Generate a sand background
      row = Array(arraySize).fill(getRandomItem([26,27]))
    } else {
      // Generate a grass background
      row = Array(arraySize).fill(getRandomItem([0,1]))
    }
    return row
  }

  private generateBackground(): number[][] {
    const gridWidth = this.sceneConfig.gridWidth
    const gridHeight = this.sceneConfig.gridHeight
    let background: number[][] = Array(gridHeight).fill([])
    let columnSize: number
    let rowSize: number
    const isMixed = getRandomInt(2) // Selects if the background will be mixed or not
    this.mixedOrientation = getRandomInt(2) // 0 = horizontal, 1 = vertical
    if (this.mixedOrientation === this.orientationTypes.HORIZONTAL) {
      // Create an horizontal tilesheet
      this.splitScenario = isMixed ? getRandomInt(gridWidth) : getRandomItem([0, gridWidth])
      columnSize = gridHeight
      rowSize = gridWidth
    } else if (this.mixedOrientation === this.orientationTypes.VERTICAL) {
      this.splitScenario = isMixed ? getRandomInt(gridHeight) : getRandomItem([0, gridHeight])
      columnSize = gridWidth
      rowSize = gridHeight
    }

    for(let i: number = 0; i < columnSize; i++) {
      // Creates the map row by row
      if( i === this.splitScenario && isMixed) {
        // Generate a row with mixed ground
        background[i] = this.generateRow(this.backgroundTypes.MIXED, rowSize)
      } else if (i < this.splitScenario ){
        background[i] = this.generateRow(this.backgroundTypes.GRASS, rowSize)
      } else {
        background[i] = this.generateRow(this.backgroundTypes.SAND, rowSize)
      }
    }
    if (this.mixedOrientation === this.orientationTypes.VERTICAL ) {
      transpose(background)
    }
    // Generates a level based on tile numbers
    return background

  }

  private createBackgroundObjects(numObjects: number = 1, objectGroupNames: string[][], collisionCategory: number): Phaser.GameObjects.Group {
    // Inserts objects in scenario
    const canvasWidth = this.sceneConfig.scene.sys.canvas.width
    const canvasHeight = this.sceneConfig.scene.sys.canvas.height
    let xMinWidth: number = 0
    let xMaxWidth: number = canvasWidth

    let yMinHeight: number = 0
    let yMaxHeight: number = canvasHeight
    const widthRatio: number = this.splitScenario / this.sceneConfig.gridWidth
    const heightRatio: number = this.splitScenario / this.sceneConfig.gridHeight

    // Filter if object types are not empty
    const objectTypes = objectGroupNames.map((obj, idx) => {
      if (obj.length > 0) {
        return idx
      } else {
        return -1
      }
    }).filter((obj) => obj !== -1)

    const group = this.sceneConfig.scene.add.group()
    const pi2 = Phaser.Math.PI2
    // Adds the sprites sequentially
    for (var i: number = 0; i < numObjects; i++) {
      // Select the type of the object group to be created, favoring grass and sand types
      // respectively
      let objectType = Phaser.Math.RND.pick(objectTypes)
      // Changes the position where the item will be positioning according to the ground // type
      if (this.mixedOrientation === this.orientationTypes.VERTICAL) {
        yMinHeight = 0
        yMaxHeight = canvasHeight
        switch (objectType) {
          case this.backgroundTypes.SAND:
            xMinWidth = Math.floor(canvasWidth*widthRatio)
            xMaxWidth = canvasWidth
            break
          case this.backgroundTypes.GRASS:
            xMinWidth = 0
            xMaxWidth = Math.floor(xMaxWidth*widthRatio)
            break
          default:
            xMinWidth = 0
            xMaxWidth = canvasWidth
            break
        }
      } else {
        xMinWidth = 0
        xMaxWidth = canvasWidth
        switch (objectType) {
          case this.backgroundTypes.SAND:
            yMinHeight = Math.floor(canvasHeight*heightRatio)
            yMaxHeight = canvasHeight
            break
          case this.backgroundTypes.GRASS:
            yMinHeight = 0
            yMaxHeight = Math.floor(yMaxHeight*heightRatio)
            break
          default:
            yMinHeight = 0
            yMaxHeight = canvasHeight
            break

        }
      }
      // Places the sprite according to their position
      let x: number = Phaser.Math.RND.between(xMinWidth, xMaxWidth)
      let y: number = Phaser.Math.RND.between(yMinHeight, yMaxHeight)
      let obj = this.sceneConfig.scene.matter.add.sprite(x, y, 'objects',
        Phaser.Math.RND.pick(objectGroupNames[objectType]))
      obj.setRotation(Math.random()*pi2)
      obj.setStatic(true)
      obj.setScale(0.6, 0.6)
      if (collisionCategory > 0) {
        obj.setCollisionCategory(collisionCategory)
        if (collisionCategory  === Number(process.env.GROUND_COLLISION_GROUP)) {
          obj.setCollidesWith(0)
        }
      }
      group.add(obj)
    }
    return group

  }

  private setBackgroundObjects(objects: objectInfo[], collisionCategory: number): Phaser.GameObjects.Group {
    const group = this.sceneConfig.scene.add.group()
    objects.forEach((obj) => {
      let objSprite = this.sceneConfig.scene.matter.add.sprite(obj.x, obj.y, 'objects', obj.sprite)
      objSprite.setRotation(obj.rotation)
      objSprite.setStatic(true)
      objSprite.setScale(0.6,0.6)
      if (collisionCategory > 0) {
        objSprite.setCollisionCategory(collisionCategory)
        if (collisionCategory  === Number(process.env.GROUND_COLLISION_GROUP)) {
          objSprite.setCollidesWith(0)
        }
      }
    })
    return group
  }

  private loadMap (level: number[][]) {
    if (this.map !== undefined) {
      this.belowPlayer.clear(true, true)
      this.levelPlayer.clear(true, true)
      this.abovePlayer.clear(true, true)
      this.map.removeAllLayers()
      this.map.destroy()
    }
    this.map = this.sceneConfig.scene.make.tilemap({ data: level,
      tileWidth: this.sceneConfig.tileWidth, tileHeight: this.sceneConfig.tileHeight })
    const tiles = this.map.addTilesetImage("tiles")
    const ground = this.map.createStaticLayer(0,tiles, 0, 0)
  }
  create(): void {
    // Create a static tile randomly based on the corresponding tiles
    this.level = this.generateBackground()
    // When loading from an array, make sure to specify the tileWidth and tileHeight
    this.loadMap(this.level)
    // Creates a set of objects on top of that
    //TODO:Find a more generic way to pass this objects on
    const groundObjects = [['treeGreen_leaf.png', 'treeGreen_twigs.png', 'treeBrown_leaf.png', 'treeBrown_twigs.png'], [], []]
    const levelObjects = [[], [], [
      'barricadeMetal.png', 'barricadeWood.png', 'sandbagBeige.png', 'sandbagBrown.png',
      'fenceRed.png', 'fenceYellow.png',
      'towerDefense_tile135.png', 'towerDefense_tile136.png', 'towerDefense_tile137.png'
    ]]
    const aboveObjects = [[
      'treeGreen_small.png', 'treeGreen_large.png', 'treeBrown_small.png', 'treeBrown_large.png', 'towerDefense_tile130.png', 'towerDefense_tile131.png'
    ], [
      'towerDefense_tile132.png', 'towerDefense_tile134.png'
    ], []]
    this.belowPlayer = this.createBackgroundObjects(5, groundObjects, Number(process.env.GROUND_COLLISION_GROUP))
    this.belowPlayer.setDepth(0, 0)
    this.levelPlayer = this.createBackgroundObjects(1, levelObjects, Number(process.env.PLAYER_COLLISION_GROUP))
    this.levelPlayer.setDepth(1, 0)
    this.abovePlayer = this.createBackgroundObjects(3, aboveObjects, Number(process.env.PLAYER_COLLISION_GROUP))
    this.abovePlayer.setDepth(2, 0)

  }

  public serializeBackground(): serializedBackground {
    const below = this.belowPlayer.getChildren().map((child: Phaser.Physics.Matter.Sprite) => {
      return {x: child.x, y: child.y, rotation: child.rotation, sprite: child.frame.name}
    })
    const level = this.levelPlayer.getChildren().map((child: Phaser.Physics.Matter.Sprite) => {
      return {x: child.x, y: child.y, rotation: child.rotation, sprite: child.frame.name}
    })
    const above = this.abovePlayer.getChildren().map((child: Phaser.Physics.Matter.Sprite) => {
      return {x: child.x, y: child.y, rotation: child.rotation, sprite: child.frame.name}
    })
    return {floor: this.level, objects: {below: below, level: level, above: above}}
  }

  public setBackground(scene: serializedBackground) {
    this.loadMap(scene.floor)
    this.level = scene.floor
    this.belowPlayer = this.setBackgroundObjects(scene.objects.below,  Number(process.env.GROUND_COLLISION_GROUP))
    this.belowPlayer.setDepth(0, 0)
    this.levelPlayer = this.setBackgroundObjects(scene.objects.level, Number(process.env.PLAYER_COLLISION_GROUP))
    this.levelPlayer.setDepth(1, 0)
    this.abovePlayer = this.setBackgroundObjects(scene.objects.above, Number(process.env.PLAYER_COLLISION_GROUP))
    this.abovePlayer.setDepth(2, 0)
  }
}