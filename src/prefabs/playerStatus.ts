import StatusBar from './statusBar'
export default class PlayerStatus {
  health: StatusBar
  power: StatusBar
  title: string
  powerOffset: {
    x: number,
    y: number

  }

  constructor(scene: Phaser.Scene, x: number, y: number, backgroundColor: number, total: number) {
    this.powerOffset = {x: 0, y: 12}
    const config = {
      x: x,
      y: y,
      total: total,
      backgroundColor: backgroundColor,
      height: 10,
      statusColor: Phaser.Display.Color.HexStringToColor('#bf3d3d').color,
      scene: scene
    }
    this.health = new StatusBar(config)
    this.power = new StatusBar({...config, x: config.x + this.powerOffset.x, y: config.y + this.powerOffset.y, statusColor: Phaser.Display.Color.HexStringToColor('#4e5bd8').color})
  }
  public currentHealth() {
    return this.health.currentValue
  }

  public currentPower() {
    return this.power.currentValue
  }

  public dropHealth(value: number) {
    this.health.dropStatus(value)
  }

  public dropPower(value: number) {
    this.power.dropStatus(value)
  }

  public updateHealth(value: number) {
    this.health.updateStatus(value)
  }

  public updatePower(value: number) {
    this.power.updateStatus(value)
  }

  public updateStatusPosition(x: number, y: number) {
    this.health.updatePosition(x, y)
    this.power.updatePosition(x + this.powerOffset.x, y + this.powerOffset.y)
  }
}