/// <reference path="../../types/phaser.d.ts" />
interface bulletProperties {
  isHeavy: boolean
  atlas: string
  x: number
  y: number
  rotation: number
  scene: Phaser.Scene
  world: Phaser.Physics.Matter.World
  tankType: string
  velocity?: number
}

export default class Bullet extends Phaser.Physics.Matter.Sprite {
  sprite: string
  config: bulletProperties
  fireVelocity: number
  isHeavy: boolean

  constructor(config: bulletProperties) {
    let sprite : string
    switch (config.tankType) {
      case 'green':
        if (config.isHeavy) {
          sprite = 'bulletGreen2_outline.png'
        } else {
          sprite = 'bulletGreen1_outline.png'
        }
        break
      case 'sand':
        if (config.isHeavy) {
          sprite = 'bulletSand2_outline.png'
        } else {
          sprite = 'bulletSand1_outline.png'
        }
        break
      case 'red':
        if (config.isHeavy) {
          sprite = 'bulletRed2_outline.png'
        } else {
          sprite = 'bulletRed1_outline.png'
        }
        break
      case 'blue':
        if (config.isHeavy) {
          sprite = 'bulletBlue2_outline.png'
        } else {
          sprite = 'bulletBlue1_outline.png'
        }
        break
      case 'dark':
        if (config.isHeavy) {
          sprite = 'bulletDark2_outline.png'
        } else {
          sprite = 'bulletDark1_outline.png'
        }
        break
      default:
        if (config.isHeavy) {
          sprite = 'bulletDark2_outline.png'
        } else {
          sprite = 'bulletDark1_outline.png'
        }
        break

    }
    // Renders the sprite
    super(config.world, config.x, config.y, config.atlas, sprite)
    this.config = config
    config.scene.add.existing(this)
    this.setDepth(1)
    this.setBounce(0)
    this.setAngularVelocity(0)
    this.setFriction(0,0,0)
    this.sprite = sprite
    this.isHeavy = config.isHeavy
    // Sets the bullet collision
    this.setCollisionCategory(Number(process.env.BULLET_COLLISION_GROUP))
    this.setCollidesWith([Number(process.env.PLAYER_COLLISION_GROUP), Number(process.env.BOUNDS_COLLISION_GROUP)])
    // Fix pivot point for bullet sprites
    this.setOriginFromFrame()
    this.updateDisplayOrigin()
    // Moves Bullet
    if (config.velocity !== undefined) {
      this.fireVelocity = config.velocity
    } else {
      this.fireVelocity = 10
    }
    this.move()
  }

  private move(): void {
    // Rotates the sprite accodingly to the weapon rotation
    this.rotation = this.config.rotation
    this.flipY = true
    // Creates the bullet velocity and sets it
    const vel = Phaser.Math.Rotate(new Phaser.Geom.Point(0,this.fireVelocity), this.config.rotation)
    this.setVelocity(vel.x, vel.y)
  }

  public hit(): void {
    let fadeDuration: number;
    // Stops bullet completely
    this.setStatic(true)
    this.setCollisionCategory(Number(process.env.GROUND_COLLISION_GROUP))
    // Play the animation explosion
    if (this.config.isHeavy) {
      this.anims.play('bigExplosion')
      fadeDuration = 500
    }
    else {
      this.anims.play('smallExplosion')
      fadeDuration = 300
    }
    // Creates a small animation on explode, making it more 'real'
    setTimeout(() => {
      this.setVisible(false)
      const trail = this.config.scene.matter.add.sprite(this.x, this.y, this.config.atlas, 'towerDefense_tile019.png')
      if(this.config.isHeavy) {
        trail.setDisplaySize(trail.width*1.2, trail.height*1.2)
      }
      trail.angle = this.angle
      trail.setStatic(true)
      trail.setDepth(0)
      trail.setCollisionCategory(Number(process.env.GROUND_COLLISION_GROUP))
      trail.setCollidesWith(0)
      const tweenConfig = {
        targets: trail,
        alpha: 0,
        ease: "Linear",
        duration: fadeDuration,
        repeat: 0,
        yoyo: false,
        delay: 1000,
        onComplete: () => {
          trail.destroy()
          this.destroy()
        }
      }
      this.config.scene.tweens.add(tweenConfig)
    }, 500)
  }

}