/// <reference path="../../types/blockly-core.d.ts" />

import { Player } from "./player";

export default class BlocklyElement {
  private static _blockly: BlocklyElement;
  public container: HTMLElement;
  workspace: Blockly.Workspace;

  public static get blockly(): BlocklyElement {
    if (this._blockly == null) {
      this._blockly = new BlocklyElement();
    }
    return this._blockly;
  }

  private constructor() {
    this.generators();
    this.container = document.getElementById('blocklyDiv');
    this.createToolbox();
    this.container.hidden = true;
  }

  getStructure() {
    let gameloops: Blockly.Block[] = Blockly.getMainWorkspace().getBlocksByType('control_gameloop', true);
    if (gameloops.length > 0) {
      let children: Blockly.Block[] = gameloops[0].getChildren(true);
      console.log(children);
    }
    Blockly.Generator['control_gameloop'];
    console.log(Blockly.JavaScript.workspaceToCode(this.workspace))
  }

  show() {
    this.container.hidden = false;
  }

  hide() {
    this.container.hidden = true;
  }

  getCode(player: Player): string {
    //let player_var = this.workspace.createVariable("player", "object");
    Blockly.VariableMap["player"] = player;
    return Blockly.JavaScript.workspaceToCode(this.workspace)
  }

  generators() {
    Blockly.JavaScript['sensor_vision'] = function (block) {
      var dropdown_visual_elements = block.getFieldValue('VISUAL_ELEMENTS');

      var code = "";
      if (dropdown_visual_elements === "0") {
        code = "player";
      } else if (dropdown_visual_elements === "1") {
        code = "projectile";
      } else if (dropdown_visual_elements === "2") {
        code = "obstacle";
      }

      // TODO: Change ORDER_NONE to the correct strength.
      return [code, Blockly.JavaScript['ORDER_NONE']];
      // return code;
    };

    Blockly.JavaScript['logic_if_vision'] = function (block) {
      var value_visual_qty = Blockly.JavaScript.valueToCode(block, 'VISUAL_QTY', Blockly.JavaScript['ORDER_ATOMIC']);
      var statements_if_statements = Blockly.JavaScript.statementToCode(block, 'IF_STATEMENTS');
      var statements_else_statements = Blockly.JavaScript.statementToCode(block, 'ELSE_STATEMENTS');
      // TODO: Assemble JavaScript into code variable.
      let player: Player = Blockly.VariableMap["player"];
      var code = ''

      // console.log(player.turret.objectData['type']+" == "+value_visual_qty+"?")
      if (player.turret.hasObjectInSight(value_visual_qty.slice(1, value_visual_qty.length - 1))) {
        code += statements_if_statements
      } else {
        code += statements_else_statements
      }
      return code;
    };

    Blockly.JavaScript['control_gameloop'] = function (block) {
      var statements_name = Blockly.JavaScript.statementToCode(block, 'NAME');
      // TODO: Assemble JavaScript into code variable.
      var code = 'this.moveForward();\n' + statements_name;
      return code;
    };

    Blockly.JavaScript['control_tank_forward'] = function (block) {
      var dropdown_tank_direction = block.getFieldValue('TANK_DIRECTION');
      // TODO: Assemble JavaScript into code variable.
      var code = '';
      if (dropdown_tank_direction === "MOVE_FORWARD") {
        code = ""
      } else {
        var angle = 0;
        if (dropdown_tank_direction === "MOVE_RIGHT") {
          angle = 45.0;
        } else if (dropdown_tank_direction === "MOVE_LEFT") {
          angle = -45.0;
        } else {
          angle = 180;
        }
        code = "this.rotateByDeltaAngle(" + angle + ", cadence);\n"
      }
      return code;
    };

    Blockly.JavaScript['control_tank_fire'] = function (block) {
      var dropdown_power = block.getFieldValue('POWER');

      var code = 'this.fireBullet(time, ' + ((dropdown_power === "LOW") ? 'false' : 'true') + ');\n';
      return code;
    };

    Blockly.JavaScript['control_rotate_gun'] = function (block) {
      var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript['ORDER_ATOMIC']);
      // TODO: Assemble JavaScript into code variable.
      var code = 'this.rotateByDeltaAngle(' + value_name + ', cadence, true);\n';
      return code;
    };

    Blockly.JavaScript['get_enemy_direction'] = function (block) {
      // TODO: Assemble JavaScript into code variable.
      var code = '...';
      // TODO: Change ORDER_NONE to the correct strength.
      return [code, Blockly.JavaScript['ORDER_NONE']];
    };

    Blockly.JavaScript['math_negate'] = function (block) {
      var value_value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript['ORDER_ATOMIC']);
      // TODO: Assemble JavaScript into code variable.
      var code = '-1*' + value_value + ';';
      // TODO: Change ORDER_NONE to the correct strength.
      return [code, Blockly.JavaScript['ORDER_NONE']];
    };

    Blockly.JavaScript['tank_variables'] = function (block) {
      var dropdown_name = block.getFieldValue('NAME');
      // TODO: Assemble JavaScript into code variable.
      var code = '...';
      // TODO: Change ORDER_NONE to the correct strength.
      return [code, Blockly.JavaScript['ORDER_NONE']];
    };
  }

  loadLibrary() {
    Blockly.defineBlocksWithJsonArray(
      [{
        "type": "sensor_vision",
        "message0": "%1",
        "args0": [
          {
            "type": "field_dropdown",
            "name": "VISUAL_ELEMENTS",
            "options": [
              [
                "inimigo",
                "0"
              ],
              [
                "projétil",
                "1"
              ],
              [
                "obstáculo",
                "2"
              ]
            ]
          }
        ],
        "inputsInline": true,
        "output": null,
        "colour": 230,
        "tooltip": "",
        "helpUrl": ""
      },
      {
        "type": "logic_if_vision",
        "message0": "Se canhão encontrou %1 %2 caso contrário %3 %4",
        "args0": [
          {
            "type": "input_value",
            "name": "VISUAL_QTY",
            "check": "VISUAL_ELEMENTS"
          },
          {
            "type": "input_statement",
            "name": "IF_STATEMENTS"
          },
          {
            "type": "input_dummy"
          },
          {
            "type": "input_statement",
            "name": "ELSE_STATEMENTS"
          }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "colour": 230,
        "tooltip": "",
        "helpUrl": ""
      },
      {
        "type": "control_gameloop",
        "message0": "game loop %1 %2",
        "args0": [
          {
            "type": "input_dummy"
          },
          {
            "type": "input_statement",
            "name": "NAME"
          }
        ],
        "colour": 0,
        "tooltip": "",
        "helpUrl": ""
      },
      {
        "type": "control_tank_forward",
        "message0": "Mover o tanque para: %1",
        "args0": [
          {
            "type": "field_dropdown",
            "name": "TANK_DIRECTION",
            "options": [
              [
                "frente",
                "MOVE_FORWARD"
              ],
              [
                "esquerda",
                "MOVE_LEFT"
              ],
              [
                "direita",
                "MOVE_RIGHT"
              ],
              [
                "ré",
                "MOVE_BACKWARD"
              ]
            ]
          }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 230,
        "tooltip": "",
        "helpUrl": ""
      },
      {
        "type": "control_tank_fire",
        "message0": "Disparar tiro %1",
        "args0": [
          {
            "type": "field_dropdown",
            "name": "POWER",
            "options": [
              [
                "fraco",
                "LOW"
              ],
              [
                "forte",
                "HIGH"
              ]
            ]
          }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 230,
        "tooltip": "",
        "helpUrl": ""
      },
      {
        "type": "control_rotate_gun",
        "message0": "Girar %1 º o canhão",
        "args0": [
          {
            "type": "input_value",
            "name": "NAME",
            "check": "Number"
          }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 230,
        "tooltip": "",
        "helpUrl": ""
      },
      {
        "type": "get_enemy_direction",
        "message0": "direção do inimigo.",
        "output": "Number",
        "colour": 230,
        "tooltip": "",
        "helpUrl": ""
      },
      {
        "type": "math_negate",
        "message0": "-1 X %1",
        "args0": [
          {
            "type": "input_value",
            "name": "VALUE",
            "check": "Number"
          }
        ],
        "output": "Number",
        "colour": 230,
        "tooltip": "",
        "helpUrl": ""
      },
      {
        "type": "tank_variables",
        "message0": "minha %1",
        "args0": [
          {
            "type": "field_dropdown",
            "name": "NAME",
            "options": [
              [
                "vida",
                "HP"
              ],
              [
                "energia",
                "ENERGY"
              ]
            ]
          }
        ],
        "output": "Number",
        "colour": 230,
        "tooltip": "",
        "helpUrl": ""
      }])

  }

  createToolbox() {
    this.loadLibrary();
    var toolbox = document.getElementById("toolbox");

    var options = {
      toolbox: toolbox,
      collapse: true,
      comments: true,
      disable: true,
      maxBlocks: Infinity,
      trashcan: true,
      horizontalLayout: false,
      toolboxPosition: 'start',
      css: true,
      media: 'https://blockly-demo.appspot.com/static/media/',
      rtl: false,
      scrollbars: true,
      sounds: true,
      oneBasedIndex: true,
      grid: {
        spacing: 20,
        length: 1,
        colour: '#888',
        snap: true
      }
    };

    /* Inject your workspace */
    this.workspace = Blockly.inject('blocklyDiv', options);

    /* Load Workspace Blocks from XML to workspace. Remove all code below if no blocks to load */

    /* TODO: Change workspace blocks XML ID if necessary. Can export workspace blocks XML from Workspace Factory. */
    var workspaceBlocks = document.getElementById("workspaceBlocks");

    /* Load blocks to workspace. */
    Blockly.Xml.domToWorkspace(workspaceBlocks, this.workspace);

  }
}
