/// <reference path="../../types/phaser.d.ts" />
import 'phaser';

export default class BaseButton extends Phaser.GameObjects.Image {
    public normalImage:string;
    public hoverImage:string;
    public callback:any;
    public clickSound:string;
    public overSound:string;
    public upImage:string;

    constructor(scene: Phaser.Scene, x: number, y: number, normalImage:string, hoverImage:string, callback:any, clickSound: string, upImage?:string, overSound?:string) {
        super(scene, x, y, normalImage);
        this.normalImage = normalImage;
        this.hoverImage = hoverImage;
        this.callback = callback;
        this.clickSound = clickSound;
        this.overSound = overSound;
        this.upImage = upImage;

        this.setInteractive();
        this.on('pointerover', this.onButtonHover);
        this.on('pointerout', this.onButtonExit);
        this.on('pointerdown', this.onButtonDown);
        this.on('pointerup', this.onButtonClick);

        this.scene.add.existing(this);
    }

    onButtonHover = (evt:any, game_object:Phaser.GameObjects.GameObject) => {
        if (this.overSound != null) {
            this.scene.sound.play(this.overSound);
        }
        this.setTexture(this.hoverImage);
    }

    onButtonExit = (evt:any, game_object:Phaser.GameObjects.GameObject) => {
        if (this.overSound != null) {
            this.scene.sound.play(this.overSound);
        }
        this.setTexture(this.normalImage);
    }

    onButtonDown = (evt:any, game_object:Phaser.GameObjects.GameObject) => {
        if (this.upImage != null) {
            this.setTexture(this.upImage);
        }
    }

    onButtonClick = (evt:any, game_object:Phaser.GameObjects.GameObject) => {
        if (this.clickSound != null)
            this.scene.sound.play(this.clickSound);
        this.callback();
    }
}