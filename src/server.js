const dotenv = require('dotenv');
const path = require('path');

let result;
let rooms = [];

let createObjectId = function () {
  var timestamp = (new Date().getTime() / 1000 | 0).toString(16);
  return timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, function() {
      return (Math.random() * 16 | 0).toString(16);
  }).toLowerCase();
};
if (process.env.NODE_ENV === "production") {
  result = dotenv.config();
} else {
  result = dotenv.config({path: path.resolve(path.join(__dirname, '..'), '.env.example')});
}
if (result.error) {
  throw result.error;
}

const app = require('express')();


app.get('/',function(req,res){
  res.sendFile(__dirname+'/index.html');
});

const server = require('http').Server(app);
const io = require('socket.io')(server);


io.on('connection', (socket) => {
  console.log("a user is connected");
  let name;
  // Create the first chat room
  if (rooms.length <= 0) {
    name = createObjectId();
    rooms.push({name: name, players: [], background: undefined});
    socket.join(name);
    socket.emit('roomCreated', name);
    console.log(`created first room ${name}`);
  } else if (rooms[rooms.length - 1].players.length >= parseInt(process.env.MAX_PLAYERS)) {
    name = createObjectId();
    rooms.push({name: name, players: [], background: undefined});
    socket.join(name);
    socket.emit('roomCreated', name);
    console.log(`max players reached for room ${rooms[rooms.length -1].name}. Created new room ${name}`);

  } else {
    name = rooms[rooms.length - 1].name;
    console.log(`assigned user to room ${name}`);
    socket.join(name);
    socket.emit('roomCreated', name);
  }
  socket.room_id = name;
  socket.on('sceneCreated', (scene) => {
    // Updates the room data
    const idx = rooms.findIndex(x => x.name === socket.room_id);
    console.log(idx)
    if (rooms[idx].background === undefined) {
      console.log(`created scene for room ${socket.room_id}`);
      rooms[idx] = {...rooms[idx], background: scene};
    } else {
      console.log("using existing background...");
    }
  });
  socket.on('playerCreated', (player) => {
    // Updates the room data
    const idx = rooms.findIndex(x => x.name === socket.room_id);
    // Syncs the players with this new one
    socket.emit('syncPlayers', rooms[idx].players);
    // Adds the new player to the players queue
    const updatedPlayer = {...player, ...{id: socket.id}};
    if (idx >= 0) {
      rooms[idx].players.push(updatedPlayer);
    }
    // Emits player to the other ones
    socket.to(socket.room_id).emit('playerJoined', updatedPlayer);
    // Emits the background to the newly added player
    socket.emit('syncBackground', rooms[idx].background);
  });
  // Handles turret movement
  socket.on('playerTurretTurn', (rotation) => {
    socket.to(socket.room_id).emit('turnTurret', {id: socket.id, rotation: rotation});
  })
  // Handles tank movement
  socket.on('playerTankMovement', (physics) => {
    socket.to(socket.room_id).emit('moveTank', {id: socket.id, physics: physics});
  })
  // Handles bullet firing
  socket.on('playerBulletFired', (isHeavy) => {

    socket.to(socket.room_id).emit('fireBullet', {id: socket.id, isHeavy: isHeavy});
  })
  // Syncs the player update
  socket.on('playerUpdated', (player) => {
    socket.to(socket.room_id).emit('updatePlayer', {id: socket.id, player: player});
  })
  // Handles the user disconnection
  socket.on('disconnect', () => {
    console.log('user disconnected');
    // Updates the room data
    const idx = rooms.findIndex(x => x.name === socket.room_id);
    if (idx >= 0) {
      rooms[idx].players = rooms[idx].players.filter((player) => player.id !== socket.id);
      // If there is not one else, deletes the room
      if(rooms[idx].players.length <= 0) {
        console.log(`deleting room ${rooms[idx].name}`);
        rooms.splice(idx, 1);
      }
    }
    io.emit('playerDisconnected', socket.id);
  });
});
server.listen(Number(process.env.SERVER_PORT), String(process.env.SERVER_HOST), () => { // Listens to port 8081
  console.log('Listening on port ' + Number(process.env.SERVER_PORT));
});