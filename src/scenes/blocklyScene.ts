import 'phaser'
import {Player} from '../prefabs/player'
import BaseButton from '../prefabs/BaseButton'
import BlocklyElement from '../prefabs/BlocklyElement'
import styles from '../../static/main.scss'

export default class blocklyScene extends Phaser.Scene{
  player: Player
  levelPlayer: any
  abovePlayer: any

  constructor() {
    super({
      key: "blocklyScene"
    });
  }

  preload(): void {
    this.load.image('bg_intro', '../assets/sprites/ss_thinktank.png')
    this.load.image("btn_blockly_normal", "../assets/sprites/ui/icon_blockly_normal.png");
    this.load.image("btn_blockly_up", "../assets/sprites/ui/icon_blockly_up.png");
    this.load.image("btn_blockly_over", "../assets/sprites/ui/icon_blockly_over.png");

    this.load.image("tiles", "../assets/sprites/tilesheet.png")
    this.load.multiatlas('objects', '../assets/sprites/objects_spritesheet.json', 'assets/sprites')
    this.load.audio('snd_hover', '../assets/audio/efeitos/EscolherCoisa02.ogg')
    this.load.audio('snd_click_voz', '../assets/audio/efeitos/Voz_Ok.ogg')
  }

  create(): void {
    // Creates a background
    this.add.image(0,0,'bg_intro')
      .setOrigin(0,0)
      .setAlpha(0.7);

    let textStyle = {color:"#333", fontSize:"22pt", fontWeight:"bolder", fontFace:styles.gameFont }
    let blockly:BaseButton = new BaseButton(this, this.sys.canvas.width-50, this.sys.canvas.height-50,"btn_blockly_normal", "btn_blockly_over", this.clickou, 'snd_click_voz', "btn_blockly_up", 'snd_hover').setOrigin(1,1).setScale(2,2);
    let okText:Phaser.GameObjects.Text = this.add.text(blockly.getCenter().x, blockly.getCenter().y, "Pronto!", textStyle).setOrigin(0.5,0.5);
    
    BlocklyElement.blockly.show();
  }

  clickou = () => {
    BlocklyElement.blockly.hide()
    this.scene.start("battleScene")
    this.scene.stop(this.scene.key)
  }
}
