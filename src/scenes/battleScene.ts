import 'phaser'
import {serializedBackground, BackgroundGenerator} from '../prefabs/backgroundGenerator'
import {Player, Turret, serializedPlayer} from '../prefabs/player'
import Bullet from '../prefabs/bullet'
import styles from '../../static/main.scss'
import * as io from "socket.io-client"
const median = require('median-quickselect')

export default class battleScene extends Phaser.Scene{
  player: Player
  blockP: Player
  enemies: Player[] = []
  background: any
  levelPlayer: any
  abovePlayer: any
  timeInSeconds: number
  totalTimeSeconds: number = 300
  timeText: Phaser.GameObjects.Text
  countDown: Phaser.Time.TimerEvent
  isMultiplayer: boolean = true
  playerCreated: boolean = false
  roomID: string
  socket: SocketIOClient.Socket
  dt: number
  private pings: number[] = []
  private latency: number = 0

  constructor() {
    super({
      key: "battleScene"
    });
  }

  preload(): void {
    this.load.image("tiles", "../assets/sprites/tilesheet.png")
    this.load.multiatlas('objects', '../assets/sprites/objects_spritesheet.json', 'assets/sprites')

    this.load.audio('snd_shot01', '../assets/audio/efeitos/Disparo02.ogg')
    this.load.audio('snd_shot02', '../assets/audio/efeitos/Disparo03.ogg')
    this.load.audio('snd_no_energy', '../assets/audio/efeitos/AcabouEnergia.ogg')
    this.load.audio('bgm_battle01', '../assets/audio/base/Base02.ogg')
  }

  private updateCountDown() {
    const addZeros = function(num: number) {
      let stringNumber = String(num)
      if (num < 10) {
          stringNumber = "0" + String(num)
      }
      return stringNumber
    }
    if (this.timeInSeconds > 0) {
      this.timeInSeconds--
      var minutes = Math.floor(this.timeInSeconds / 60)
      var seconds = this.timeInSeconds - (minutes * 60)
      var timeString = addZeros(minutes) + ":" + addZeros(seconds)
      this.timeText.text = timeString
      if (this.timeInSeconds < Math.ceil(this.totalTimeSeconds * 0.5) && this.timeInSeconds > 10) {
        this.timeText.setColor('#e2b900')
      } else if (this.timeInSeconds <= 10) {
        this.timeText.setColor('#b70101')
      }
    } else {
      // TODO: Ends the current game and moves to another screen
      this.scene.stop()
    }
  }

  private createCountDown() {
    this.timeInSeconds = this.totalTimeSeconds
    this.timeText = this.add.text(Math.floor(this.sys.canvas.width / 2) - 100, 20, "",
    {font: `5em`, fill: '#FFFFFF'})
    this.timeText.setFontFamily(styles.gameFont)
    this.countDown = this.time.addEvent({
      loop: true,
      delay: 1000,
      callback: this.updateCountDown,
      callbackScope: this
    })
    this.timeText.setDepth(100)
    this.timeText.setAlign('center')
  }

  private changeBGM ( bgm_key: string ) {
    this.game.sound.stopAll()
    this.game.sound.play(bgm_key, {loop: true});
  }

  create(): void {
    // Create the countdown
    //this.createCountDown()
    // setting Matter world bounds
    this.matter.world.setBounds(0, -10, this.sys.canvas.width,this.sys.canvas.height + 10)
    this.changeBGM('bgm_battle01');

    // Creates a background
    this.background = new BackgroundGenerator({gridHeight: 10, gridWidth: 10, tilesheetPath: "../assets/sprites/tilesheet.png",
      tileWidth: 128, tileHeight: 128, spritesheetAtlas: '../assets/sprites/objects_spritesheet.json', scene: this})

    // create all explosion animations
    const smallExplosionFrames = this.anims.generateFrameNames('objects', {prefix: 'explosionSmoke', start: 1, end: 5, suffix: '.png'})
    const bigExplosionFrames = this.anims.generateFrameNames('objects', {prefix: 'explosion', start: 1, end: 5, suffix: '.png'})
    this.anims.create({key: 'smallExplosion', frames: smallExplosionFrames, repeat: 0, duration: 500})
    this.anims.create({key: 'bigExplosion', frames: bigExplosionFrames, repeat: 0, duration: 500})
    this.anims.create({key: 'tankExplosion', frames: bigExplosionFrames, repeat: 0, duration: 1000})
    // Adds the bullet collision handler
    this.matter.world.on('collisionstart', this.bulletCollision)
    this.matter.world.on('collisionstart', this.sensorCollision)
    this.matter.world.on('collisionend', this.sensorCollision)

    this.blockP = new Player({
      scene: this,
      world: this.matter.world,
      x: Phaser.Math.RND.between(0, this.sys.canvas.width),
      y: Phaser.Math.RND.between(0, this.sys.canvas.height),
      atlas: 'objects',
      sprite: 'tankBody_blue_outline.png',
      turretSprite: 'tankBlue_barrel3_case.png',
      trailSprite: 'tracksSmall.png',
      socket: this.socket,
      isMultiplayer: false
    }, true);

    if (this.isMultiplayer) {
      // Connect to the socket io server
      this.socket = io.connect(`${String(process.env.SERVER_HOST)}:${Number(process.env.SERVER_PORT)}`)
      // Estimates the player latency to the server
      this.socket.on('pong', (latency: number) => {
        this.pings.push(latency)
        if(this.pings.length > 20) this.pings.shift()
        this.latency = median(this.pings)
      })
      // Adds listener for creating the room
      this.socket.on('roomCreated', (roomID: string) => {
        this.roomID = roomID
      })
      // In case a new player joins the party
      this.socket.on('playerJoined', (newPlayer: serializedPlayer) => {
        this.addEnemy(newPlayer)
      })
      // Sync all players
      this.socket.on('syncPlayers', (players: serializedPlayer[]) => players.forEach((enemy: serializedPlayer) => {this.addEnemy(enemy)}))
      // Sync the background
      this.socket.on('syncBackground', (scene: serializedBackground) => {
        this.background.setBackground(scene)
      })
      this.socket.on('playerDisconnected', (playerID: string) => this.disconnectPlayer(playerID))
      // Updates the player

      this.socket.on('updatePlayer', (playerInfo: any) => {
        let tweenConfig = {
          x: playerInfo.player.x,
          y: playerInfo.player.y,
          ease: "Linear",
          duration: Math.max(200 - 2*this.latency, 0),
          repeat: 0,
          yoyo: false,
          delay: 0
        }
        if(playerInfo.id === this.player.id) {

          this.add.tween({...tweenConfig, ...{
            targets:this.player,
            onComplete: () => {
              this.player.status.updateHealth(playerInfo.player.status.currentHealth)
              this.player.status.updatePower(playerInfo.player.status.currentPower)
              this.player.isDestroyed = playerInfo.player.status.isDestroyed
              this.player.energyDepleted = playerInfo.player.status.energyDepleted
            }
          }})
        } else {
          const enemyIDX = this.enemies.findIndex((obj) => obj.id === playerInfo.id)
          if (enemyIDX >= 0) {
            this.add.tween({...tweenConfig, ...{
              targets:this.enemies[enemyIDX],
              onComplete: () => {
                this.enemies[enemyIDX].status.updateHealth(playerInfo.player.status.currentHealth)
                this.enemies[enemyIDX].status.updatePower(playerInfo.player.status.currentPower)
                this.enemies[enemyIDX].isDestroyed = playerInfo.player.status.isDestroyed
                this.enemies[enemyIDX].energyDepleted = playerInfo.player.status.energyDepleted
              }
            }})
          }
        }
      })
      // fires bullet
      this.socket.on('fireBullet', (bulletInfo: any) => {
        if(bulletInfo.id === this.player.id) {
          this.player.fireBullet(this.dt + this.player.nextFire, bulletInfo.isHeavy)
        } else {
          const enemyIDX = this.enemies.findIndex((obj) => obj.id === bulletInfo.id)
          if (enemyIDX >= 0) {
            this.enemies[enemyIDX].fireBullet(this.dt + this.enemies[enemyIDX].nextFire, bulletInfo.isHeavy)
          }
        }
      })
      // Capture turret movements
      this.socket.on('turnTurret', (turretInfo: any) => {
        if(turretInfo.id === this.player.id) {
          this.player.updateTurretRotation(this.dt, undefined, undefined, turretInfo.rotation)
        } else {
          const enemyIDX = this.enemies.findIndex((obj) => obj.id === turretInfo.id)
          if (enemyIDX >= 0) {
            this.enemies[enemyIDX].updateTurretRotation(this.dt, undefined, undefined, turretInfo.rotation)
          }
        }
      })
      // Capture tank movements
      this.socket.on('moveTank', (tankInfo: any) => {
        if(tankInfo.id === this.player.id) {
          this.player.setVelocity(tankInfo.physics.velocity.x, tankInfo.physics.velocity.y)
          if (tankInfo.physics.velocity.x !== 0 || tankInfo.physics.velocity.y !== 0) {
            this.player.isMoving = true
          } else {
            this.player.isMoving = false
          }
        } else {
          const enemyIDX = this.enemies.findIndex((obj) => obj.id === tankInfo.id)
          if (enemyIDX >= 0) {
            this.enemies[enemyIDX].setVelocity(tankInfo.physics.velocity.x, tankInfo.physics.velocity.y)
            this.enemies[enemyIDX].updateRotation(this.dt, tankInfo.physics.rotation)
            if (tankInfo.physics.velocity.x !== 0 || tankInfo.physics.velocity.y !== 0) {
              this.enemies[enemyIDX].isMoving = true
            } else {
              this.enemies[enemyIDX].isMoving = false
            }
          }
        }
      })
    }
    // adds the player
    this.player = new Player({
      scene: this,
      world: this.matter.world,
      x: Phaser.Math.RND.between(0, this.sys.canvas.width),
      y: Phaser.Math.RND.between(0, this.sys.canvas.height),
      atlas: 'objects',
      sprite: 'tankBody_red_outline.png',
      turretSprite: 'tankRed_barrel2_case.png',
      trailSprite: 'tracksSmall.png',
      socket: this.socket,
      isMultiplayer: this.isMultiplayer
    })
  }

  private disconnectPlayer(playerID: string) {

    // Removes disconnected player
    const playerIdx = this.enemies.findIndex((player) => player.id === playerID)
    if (playerIdx >= 0) {
      // clean player radar
      this.player.turret.sensorOn = false
      this.player.turret.cleanSensor()
      this.enemies[playerIdx].destroy()
      this.enemies.splice(playerIdx, 1)
      // Turn player radar back on
      this.player.turret.sensorOn = true
    }
  }

  update(time:number, delta: number): void {
    this.dt = delta
    if (this.roomID !== undefined && !this.playerCreated) {
      this.playerCreated = true
      this.socket.emit('sceneCreated', this.background.serializeBackground())
      this.socket.emit('playerCreated', this.player.serializePlayer())
    }
    // Updates the player
    this.player.update(time, delta)
    // Updates the enemies
    this.enemies.forEach((enemy) => enemy.update(time, delta))
    // update npc (blockly)
    this.blockP.update(time, delta);
  }

  private addEnemy(newPlayer: serializedPlayer) {
    const enemy = new Player({
      scene: this,
      world: this.matter.world,
      x: newPlayer.x,
      y: newPlayer.y,
      atlas: 'objects',
      sprite: newPlayer.sprite,
      turretSprite: newPlayer.turret.sprite,
      trailSprite: newPlayer.trail,
      socket: this.socket,
      isMultiplayer: true,
      id: newPlayer.id
    }, true)
    this.enemies.push(enemy)
  }

  private sensorCollision(event: any, bodyA: any, bodyB: any) {
    let sensor: any
    let object: any
    if (bodyA.gameObject instanceof Turret) {
      sensor = bodyA
      object = bodyB
    } else if (bodyB.gameObject instanceof Turret) {
      sensor = bodyB
      object = bodyA
    }
    if (sensor !== undefined) {
      // Checks if sensor collided with the object front
      const bottom = sensor.gameObject.getBottomRight()
      const bottomDistance = Phaser.Math.Distance.Between(bottom.x, bottom.y, object.position.x, object.position.y)
      const sensorDistance = Phaser.Math.Distance.Between(sensor.position.x, sensor.position.y, object.position.x, object.position.y)
      if (bottomDistance <= sensorDistance) {
        console.debug("collided front!")
        if (event.name === "collisionStart") {
          sensor.gameObject.trackObject(object.gameObject)
        } else if (event.name === "collisionEnd") {
          sensor.gameObject.forgetObject(object.gameObject)
        }
      }

    }
  }
  private bulletCollision(event: any, bodyA: any, bodyB: any) {
    let bullet: any
    let tank: any
    if(bodyA.gameObject instanceof Bullet) {
      bullet = bodyA
    } else if (bodyB.gameObject instanceof Bullet) {
      bullet = bodyB
    }
    if (bullet !== undefined) {
      console.debug('Bullet destroyed')
      bullet.gameObject.hit()
    }

    if(bodyA.gameObject instanceof Player) {
      tank = bodyA
    } else if (bodyB.gameObject instanceof Player) {
      tank = bodyB
    }
    if (tank !== undefined && bullet !== undefined) {
      // Tank was hit by bullet
      tank.gameObject.takeHit(bullet.gameObject)
    }
  }
}
