import 'phaser'
import BaseButton from '../prefabs/BaseButton';

export default class introScene extends Phaser.Scene{
  
  constructor() {
    super({
      key: "introScene"
    });
  }

  preload() {
    this.load.image('bg_intro', '../assets/sprites/ss_thinktank.png')
    this.load.image('btn_jogar_normal', '../assets/sprites/ui/jogar_normal.png')
    this.load.image('btn_jogar_hover', '../assets/sprites/ui/jogar_over.png')
    this.load.image('btn_jogar_click', '../assets/sprites/ui/jogar_click.png')

    this.load.audio('bgm_menu', '../assets/audio/base/Base01.ogg')
    this.load.audio('snd_click', '../assets/audio/efeitos/EscolherCoisa01.ogg')
    this.load.audio('snd_hover', '../assets/audio/efeitos/EscolherCoisa02.ogg')
  }

  create() {
    this.game.sound.play('bgm_menu', {loop: true});
    this.add.image(0,0,'bg_intro')
      .setOrigin(0,0)
      .setAlpha(0.7);

    let btn:BaseButton = new BaseButton(this, this.sys.canvas.width/2, this.sys.canvas.height/2, 'btn_jogar_normal', 'btn_jogar_hover', this.clickJogar, 'snd_click', 'btn_jogar_click', 'snd_hover')
  }

  clickJogar = () => {
    this.scene.stop('introScene')
    this.scene.start('blocklyScene')
  }
}