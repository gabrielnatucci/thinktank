/// <reference path="../types/phaser.d.ts" />
/// <reference path="../types/blockly-core.d.ts" />
/// <reference path="../types/declaration.d.ts" />
import 'phaser'
import battleScene from './scenes/battleScene'
import blocklyScene from './scenes/blocklyScene'
import styles from '../static/main.scss'
import introScene from './scenes/introScene';

let config = {
  type: Phaser.AUTO,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    parent: "gamediv",
    width: 1280,
    height: 960,
  },
  physics: {
    default: 'matter',
    matter: {
      //debug: process.env.NODE_ENV === "development",
      gravity: {
        x: 0,
        y: 0
      }
    }
  },
  scene: [introScene, battleScene, blocklyScene],
  title: 'Virtual Pet',
  backgroundColor: styles.backgroundColor
}

// create the game, and pass it the configuration
let game = new Phaser.Game(config)
game.events.on('hidden', function () {
  console.log('hidden');
}, this);

game.events.on('visible', function () {
  console.log('visible');
}, this);
