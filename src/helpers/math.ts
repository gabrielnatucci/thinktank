
export function getRandomInt(max: number): number {
  return Math.floor(Math.random() * Math.floor(max));
}

export function getRandomItem(myArray: any[]): any {
  return myArray[Math.floor(Math.random()*myArray.length)]
}

export function transpose(matrix: number[][]): void {
  for (var i = 0; i < matrix.length; i++) {
    for (var j = 0; j < i; j++) {
      const temp = matrix[i][j];
      matrix[i][j] = matrix[j][i];
      matrix[j][i] = temp;
    }
  }
}